# Raspberry Pi Defaults
class profile::raspbian{
  notify{ 'Hello World' : }
  cron::job { 'puppet_agent_service' :
    ensure  => 'present',
    command => '/opt/puppetlabs/bin/puppet agent --onetime --no-daemonize --splay --splaylimit 60',
    user    => 'root',
    minute  => 30,    
  }
}
